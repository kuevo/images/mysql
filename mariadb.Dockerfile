# syntax=docker/dockerfile:1.4
ARG DB_VERSION="10.7"
FROM library/mariadb:${DB_VERSION}
ARG DB_VERSION="10.7"
COPY --link config/mariadb${DB_VERSION}.cnf /etc/mysql/conf.d/custom.cnf
ARG apt update && \
    apt upgrade -y && \
    apt install -y python3-pip && \
    pip3 install --upgrade pip && \
    pip3 install mycli && \
    chmod 644 /etc/mysql/conf.d/custom.cnf

VOLUME [ "/var/lib/mysql" ]
